import numpy as np
import time
import pandas as pd
from pathlib import Path

from models.PODDeepOnet import *
<<<<<<< HEAD
<<<<<<< HEAD:tests/SPQN_DON_test/Advection/DON_test_ASPQN.py
from models.ResNetDenseConstantWidth import *
from datasets.DONDatasets import AdvectionDONDataset
=======
from datasets.DONDatasets import AniDiffDONDataset
from models.ResNetDenseConstantWidth import *
>>>>>>> 0d25d4ef251e191c62c62d5579e3fd3270232d8f:tests/SPQN_DON_test/AniDiff/PDON_test_ASPQN.py
=======
from models.ResNetDenseConstantWidth import *
from datasets.DONDatasets import AdvectionDONDataset
>>>>>>> 0d25d4ef251e191c62c62d5579e3fd3270232d8f
from trainers.SPQNTrainerRight import *


# python3 test_SPQN_twolevel_additive_serial.py  --num_points_x 25 --num_points_y 25 --num_ref 2 --num_subdomains 6 --lr_global 1  --hiddenlayers_coarse 4 --lr_local 1.0 --max_epochs 1000 --use_adaptive_activation True --num_levels 1 world_size  1  args.num_subdomains  6

args = get_params()
torch.set_default_dtype(torch.float64)

world_size = 1
rank = 0

print("world_size ", world_size, " args.num_subdomains ", args.num_subdomains)
print(" \n \n args ", args, " \n \n ")

print(" - - - ASPQN  - DeepOnet test  - - -")

np.random.seed(args.seed)
torch.manual_seed(args.seed)
torch.cuda.manual_seed(args.seed)
np.random.seed(args.seed)


<<<<<<< HEAD
<<<<<<< HEAD:tests/SPQN_DON_test/Advection/DON_test_ASPQN.py
dataset = AdvectionDONDataset("train_IC2.npz", "test_IC2.npz", eval_PCA=True)
=======
dataset = AniDiffDONDataset("Anisotropic_5000_1_40_1.pkl", eval_PCA=True)
>>>>>>> 0d25d4ef251e191c62c62d5579e3fd3270232d8f:tests/SPQN_DON_test/AniDiff/PDON_test_ASPQN.py
=======
dataset = AdvectionDONDataset("train_IC2.npz", "test_IC2.npz", eval_PCA=True)
>>>>>>> 0d25d4ef251e191c62c62d5579e3fd3270232d8f

branch_net = ResNetDenseConstantWidth(  inputs=dataset.train_set.data_set[0].shape[1],\
                                        outputs=dataset.num_pca_basis,\
                                        hiddenlayers=args.hiddenlayers_coarse,\
                                        width=args.width,\
                                        dt=args.T/(args.hiddenlayers_coarse-1),\
                                        act_fun="relu",\
                                        use_adaptive_activation=False)

<<<<<<< HEAD
=======

>>>>>>> 0d25d4ef251e191c62c62d5579e3fd3270232d8f
net     = PODDeepOnet(POD_basis = dataset.pca_basis, POD_mean = dataset.pca_mean, branch_net=branch_net)
net.print_decomposition(args.num_subdomains)


elapsed_time = 0
epoch = 0
train_loss = 0
converged = False


if(args.use_SPQN):
  dir_name = "ASPQN_"+ "_width_"+str(args.width) + "_num_coarse_steps_"+str(args.num_coarse_steps)
else:
  dir_name = "BFGS_"+"_width_"+str(args.width) + "_num_coarse_steps_"+str(args.num_coarse_steps)


Path(dir_name).mkdir(parents=True, exist_ok=True)

trainer = ASPQNTrainerRight(training_pinn_flg=False,
                            num_subdomains=args.num_subdomains,
                            type="regression",
                            freq_print=1,
                            use_wandb=False,
                            use_coarse=True, 
<<<<<<< HEAD
                            layerwise_lr_update=True)
=======
                            layerwise_lr_update=args.use_layerwise_lr)
>>>>>>> 0d25d4ef251e191c62c62d5579e3fd3270232d8f


trainer.config["max_epochs"] = args.max_epochs
trainer.config["conv_history_csv_name"] = dir_name + "/" + \
                                          'Sum_seed_' + str(args.seed)+\
                                          "_num_coarse_steps_" + str(args.num_coarse_steps) +\
<<<<<<< HEAD
                                          '_lr_global_'+str(args.lr_global) +\
                                          '_lrl_local_'+str(args.lr_local) +\
                                          '_num_local_steps_'+str(args.num_local_steps)+\
                                          "_max_epochs_" + str(args.max_epochs) +\
                                          "_num_points_x_"+str(args.num_points_x)+\
                                          "_num_points_y_"+str(args.num_points_y)+\
                                          "_num_ref_"+str(args.num_ref)+\
                                          "_num_subdomains_"+str(args.num_subdomains)+\
                                          "_width_"+str(args.width)+\
                                          "_hiddenlayers_coarse_"+str(args.hiddenlayers_coarse)+\
                                          "_ada_net_"+str(args.ada_net)+\
                                          "_SPQN_"+str(args.use_SPQN)
=======
                                          '_num_local_steps_'+str(args.num_local_steps)+\
                                          "_max_epochs_" + str(args.max_epochs) +\
                                          "_num_subdomains_"+str(args.num_subdomains)+\
                                          "_width_"+str(args.width)+\
                                          "_hiddenlayers_coarse_"+str(args.hiddenlayers_coarse)+\
                                          "_SPQN_"+str(args.use_SPQN)+\
                                          "_overlap_width_"+str(args.overlap_width)+\
                                          "_use_layerwise_lr_"+str(args.use_layerwise_lr)
>>>>>>> 0d25d4ef251e191c62c62d5579e3fd3270232d8f

start_time = time.time()


def criterion(u_pred, u_true):
<<<<<<< HEAD
  loss =  (torch.mean((u_pred - u_true) ** 2 / (u_true ** 2 + 1E-4)))
=======
  loss =  torch.mean((u_pred - u_true) ** 2)  # MSE
  # loss =  (torch.mean((u_pred - u_true) ** 2 / (u_true ** 2 + 1E-4)))
>>>>>>> 0d25d4ef251e191c62c62d5579e3fd3270232d8f
  return loss


convergence_status = trainer.train( dataset=dataset,
                                    net=net,
                                    criterion=criterion,
                                    global_optimizer_type=PrecLBFGS,
                                    local_optimizer_type=PrecLBFGS,
                                    args=args)


elapsed_time = time.time() - start_time

out_name = dir_name + "/" + 'Summary.csv'

df = pd.DataFrame({'seed': [args.seed],
                 'use_SPQN':[args.use_SPQN],
                 'num_coarse_steps': [args.num_coarse_steps],
                 'num_points_x':[args.num_points_x],
                 'num_points_y':[args.num_points_y],
                 'num_ref':[args.num_ref],
                 'num_levels': [args.num_levels],
                 'width': [args.width],
                 'hiddenlayers_coarse': [args.hiddenlayers_coarse],
                 'num_subdomains': [args.num_subdomains],
                 'num_local_steps':[args.num_local_steps],
                 'ada_net': [args.ada_net],
                 'use_adaptive_activation':[args.use_adaptive_activation],
                 'T': [args.T],
                 'lr_global': [args.lr_global],
                 'lr_local': [args.lr_local],
                 'history': [args.history],
                 'time': [elapsed_time],
                 'epochs': [convergence_status["epochs"]],
                 'l_evals': [convergence_status["num_loss_evals"]],
                 'g_evals': [convergence_status["num_grad_evals"]],
                 'loss': [convergence_status["loss_train"]],
                 'loss_test': [convergence_status["loss_val"]],
                 'L2_error': [convergence_status["L2_error"]],
                 'L2_error_rel': [convergence_status["L2_error_rel"]],
                 'overlap_width': [args.overlap_width]})

if Path(out_name).exists():
    df.to_csv(out_name, index=False, header=False, mode='a')
else:
    df.to_csv(out_name, index=False, header=True, mode='a')



