#!/bin/bash
mkdir BFGS

# for seed in 13 125 1;
for seed in 1 13;
do
	for max_epochs in 10000000;
	do 		
		for num_coarse_steps in 25;
		do 				
			for history in 3;
			do 		
				for num_subdomains in 3;
				do 										
					for num_local_steps in 50;
					do																		
						for hiddenlayers_coarse in 4;
						do 		
							for width in 256;
							do 												
								for T in 4;
								do 												
									sbatch --output=BFGS/TL_ASM_num_local_steps${num_local_steps}_seed${seed}_num_coarse_steps${num_coarse_steps}_num_subdomains${num_subdomains}_max_epochs${max_epochs}_T${T}_numlayers_${hiddenlayers_coarse}_width${width}.out --job-name=TL_ASM submission_script_BFGS.job $seed $max_epochs $num_coarse_steps $history $num_subdomains $hiddenlayers_coarse $width $T $num_local_steps; 
								done
							done
						done
					done
				done
			done
		done
	done
done
