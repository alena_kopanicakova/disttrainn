#!/bin/bash

# for seed in 13;
for seed in 13 125 1 546 722;
do
	for max_epochs in 200;
	do 		
		for num_coarse_steps in 0;
		do 				
			for history in 3;
			do 		
				for num_subdomains in 5 3 2;
				do 										
					for num_local_steps in 50;
					do																		
						for hiddenlayers_coarse in 6;
						do 		
							for width in 64;
							do 												
								for T in 6;
								do 					
									for overlap_width in 0 1 2;
									do
										for use_layerwise_lr in True False;
										do
											sbatch --output=TL_ASM_num_local_steps${num_local_steps}_seed${seed}_num_coarse_steps${num_coarse_steps}_num_subdomains${num_subdomains}_max_epochs${max_epochs}_T${T}_numlayers_${hiddenlayers_coarse}_width${width}_use_layerwise_lr${use_layerwise_lr}.out --job-name=TL_ASM submission_script_ASPQN_oscar2.job $seed $max_epochs $num_coarse_steps $history $num_subdomains $hiddenlayers_coarse $width $T $num_local_steps $overlap_width $use_layerwise_lr; 
										done
									done
								done
							done
						done
					done
				done
			done
		done
	done
done
