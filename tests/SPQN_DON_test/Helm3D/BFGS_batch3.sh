#!/bin/bash

for seed in 125 1 546 722;
# for seed in 13 125 1 546 722;
do
	for max_epochs in 10000;
	do 		
		for num_coarse_steps in 50;
		do 				
			for history in 3;
			do 		
				for num_subdomains in 2;
				do 										
					for num_local_steps in 25;
					do																		
						for hiddenlayers_coarse in 8;
						do 		
							for width in 40;
							do 												
								for T in 8;
								do 					
									for overlap_width in 1;
									do
										sbatch --output=BFGS_num_local_steps${num_local_steps}_seed${seed}_num_coarse_steps${num_coarse_steps}_num_subdomains${num_subdomains}_max_epochs${max_epochs}_T${T}_numlayers_${hiddenlayers_coarse}_width${width}.out --job-name=TL_ASM submission_script_BFGS_oscar3.job $seed $max_epochs $num_coarse_steps $history $num_subdomains $hiddenlayers_coarse $width $T $num_local_steps $overlap_width; 
									done
								done
							done
						done
					done
				done
			done
		done
	done
done
