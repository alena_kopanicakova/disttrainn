import numpy as np
import time
import pandas as pd
from pathlib import Path

from models.DeepOnet import *
from datasets.DONDatasets import Helm3DDONDataset
from models.ResNetDenseConstantWidth import *
from models.CNN3D import *
from trainers.SPQNTrainerRight_parallel import *

import torch.multiprocessing as mp

# def main():

#   # args = get_params()
#   # setup_distr_env()

#   # args.nodes = 1
#   # args.gpus = torch.cuda.device_count()
#   # args.world_size = args.gpus * args.nodes
#   # mp.spawn(dist_train, nprocs=args.gpus, args=(args,))
  
#   args = get_params()
#   torch.set_default_dtype(torch.float64)


#   setup_distr_env()
#   dist.init_process_group(backend="nccl")
#   world_size = dist.get_world_size()
#   rank = dist.get_rank()


def main():
  # dist.init_process_group(backend="nccl", init_method='env://', world_size=args.world_size, rank=gpu)
  # world_size = dist.get_world_size()
  # rank = dist.get_rank()

  # print("world_size==num subdomains ", world_size)
  # print("world_size : ", args.world_size, "rank : ", rank, "gpu : ", gpu)
  # torch.set_default_dtype(torch.float64)
  # torch.cuda.set_device(gpu)




  args = get_params()
  torch.set_default_dtype(torch.float64)


  # exit(0)  


  setup_distr_env()
  dist.init_process_group(backend="nccl")
  world_size = dist.get_world_size()
  rank = dist.get_rank()

  print("world_size==num subdomains ", world_size)


  if(rank==0):
    print(" \n \n args ", args, " \n \n ")

  # exit(0)  



  np.random.seed(args.seed)
  torch.manual_seed(args.seed)
  torch.cuda.manual_seed(args.seed)
  np.random.seed(args.seed)
  torch.backends.cudnn.benchmark = True

  # dataset = Helm3DDONDataset("NonNestedHelm3D_5000_1_32_1_0.0001.pkl", eval_PCA=False)
  dataset = Helm3DDONDataset(eval_PCA=False)

  

  # decompose total number of layers proportial to 1:2
  # hidden_trunk = args.hiddenlayers_coarse // 3
  # hidden_branch = args.hiddenlayers_coarse - hidden_trunk
  hidden_trunk = 1
  hidden_branch = 7
  num_basis_functions = 128

  branch_net = CNN3D( inputs=dataset.train_set.data_set[0].shape[1],\
                      outputs=num_basis_functions,\
                      hiddenlayers=hidden_branch,\
                      width=args.width,\
                      act_fun="relu",\
                      use_adaptive_activation=False)

  trunk_net = ResNetDenseConstantWidth( inputs=dataset.train_coordinate.shape[1],\
                                        outputs=num_basis_functions,\
                                        hiddenlayers=hidden_trunk,\
                                        width=2*args.width,\
                                        dt=args.T/(args.hiddenlayers_coarse-1),\
                                        act_fun="tanh",\
                                        use_adaptive_activation=False)

  net     = DeepOnet(branch_net=branch_net, trunk_net=trunk_net, trunk_net_input=dataset.train_coordinate)
  net.print_decomposition(args.num_subdomains)

  elapsed_time = 0
  epoch = 0
  train_loss = 0
  converged = False


  if(args.use_SPQN):
    dir_name = "ASPQN_"+ "_width_"+str(args.width) + "_num_coarse_steps_"+str(args.num_coarse_steps)
  else:
    dir_name = "BFGS_"+"_width_"+str(args.width) + "_num_coarse_steps_"+str(args.num_coarse_steps)


  Path(dir_name).mkdir(parents=True, exist_ok=True)

  trainer = ASPQNTrainerRightDistrib( training_pinn_flg=False,
                                      num_subdomains=args.num_subdomains,
                                      type="regression",
                                      freq_print=1,
                                      use_wandb=False,
                                      use_coarse=True, 
                                      layerwise_lr_update=True)

  args.model_name     =  "DON_Helm_"+str(args.num_subdomains) + "_SEED_" + str(args.seed) + "_use_SPQN_" + str(args.use_SPQN)
  trainer.load_model(net, args)  

  trainer.config["max_epochs"] = args.max_epochs
  trainer.config["conv_history_csv_name"] = dir_name + "/" + \
                                            'Sum_seed_' + str(args.seed)+\
                                            "_num_coarse_steps_" + str(args.num_coarse_steps) +\
                                            '_lr_global_'+str(args.lr_global) +\
                                            '_lrl_local_'+str(args.lr_local) +\
                                            '_num_local_steps_'+str(args.num_local_steps)+\
                                            "_max_epochs_" + str(args.max_epochs) +\
                                            "_num_points_x_"+str(args.num_points_x)+\
                                            "_num_points_y_"+str(args.num_points_y)+\
                                            "_num_ref_"+str(args.num_ref)+\
                                            "_num_subdomains_"+str(args.num_subdomains)+\
                                            "_width_"+str(args.width)+\
                                            "_hiddenlayers_coarse_"+str(args.hiddenlayers_coarse)+\
                                            "_ada_net_"+str(args.ada_net)+\
                                            "_SPQN_"+str(args.use_SPQN)

  start_time = time.time()


  def criterion(u_pred, u_true):
    loss =  torch.mean((u_pred - u_true) ** 2)  # MSE
    # loss =  torch.mean((u_pred - u_true) ** 2 / (u_true ** 2 + 1E-4))
    return loss


  convergence_status = trainer.train( dataset=dataset,
                                      net=net,
                                      criterion=criterion,
                                      global_optimizer_type=PrecLBFGS,
                                      local_optimizer_type=PrecLBFGS,
                                      args=args)


  elapsed_time = time.time() - start_time

  out_name = dir_name + "/" + 'Summary.csv'

  df = pd.DataFrame({'seed': [args.seed],
                  'use_SPQN':[args.use_SPQN],
                  'num_coarse_steps': [args.num_coarse_steps],
                  'num_points_x':[args.num_points_x],
                  'num_points_y':[args.num_points_y],
                  'num_ref':[args.num_ref],
                  'num_levels': [args.num_levels],
                  'width': [args.width],
                  'hiddenlayers_coarse': [args.hiddenlayers_coarse],
                  'num_subdomains': [args.num_subdomains],
                  'num_local_steps':[args.num_local_steps],
                  'ada_net': [args.ada_net],
                  'use_adaptive_activation':[args.use_adaptive_activation],
                  'T': [args.T],
                  'lr_global': [args.lr_global],
                  'lr_local': [args.lr_local],
                  'history': [args.history],
                  'time': [elapsed_time],
                  'epochs': [convergence_status["epochs"]],
                  'l_evals': [convergence_status["num_loss_evals"]],
                  'g_evals': [convergence_status["num_grad_evals"]],
                  'loss': [convergence_status["loss_train"]],
                  'loss_test': [convergence_status["loss_val"]],
                  'L2_error': [convergence_status["L2_error"]],
                  'L2_error_rel': [convergence_status["L2_error_rel"]],
                  'overlap_width': [args.overlap_width]})

  if(dist.get_rank()==0):
    if Path(out_name).exists():
        df.to_csv(out_name, index=False, header=False, mode='a')
    else:
        df.to_csv(out_name, index=False, header=True, mode='a')

if __name__ == '__main__':
  main()



