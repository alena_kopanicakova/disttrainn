#!/bin/bash

export PYTHONPATH="${PYTHONPATH}:/users/ylee285/scratch/disttrainn"
export PYTHONPATH="${PYTHONPATH}:/Users/lua/research/disttrainn"

for seed in 13;
# for seed in 13 125 1 546 722;
do
	for max_epochs in 10000;
	do 		
		for num_coarse_steps in 50;
		do 				
			for history in 3;
			do 		
				for num_subdomains in 2;
				do 										
					for num_local_steps in 50;
					do																		
						for hiddenlayers_coarse in 8;
						do 		
							for width in 40;
							do 												
								for T in 8;
								do 					
									python VDON_test_ASPQN.py --seed ${seed} --max_epochs ${max_epochs} --num_coarse_steps ${num_coarse_steps} --history ${history} --num_subdomains ${num_subdomains} --hiddenlayers_coarse ${hiddenlayers_coarse} --width ${width} --T ${T} --num_local_steps ${num_local_steps} --use_SPQN False;
								done
							done
						done
					done
				done
			done
		done
	done
done
