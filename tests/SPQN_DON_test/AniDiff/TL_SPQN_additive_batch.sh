#!/bin/bash
mkdir TL_BFGS

for seed in 125 1 546;
do
	sbatch --output=TL_BFGS/TL_ASM_seed${seed}_seed${seed}.out --job-name=TL_ASM submission_script_TL_ASPQN_daint.job $seed; 									
done