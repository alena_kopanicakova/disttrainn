import pandas as pd
import time
import warnings
import scipy.stats
import numpy as np
import matplotlib.pyplot as plt
import glob
import argparse
import logging
import os
import sys
sys.setrecursionlimit(1000000)

# to ignore warnings ...
warnings.filterwarnings('ignore')

logging.disable(logging.WARNING)
os.environ["TF_CPP_MIN_LOG_LEVEL"] = "3"

# Just disables the warning, doesn't enable AVX/FMA
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'


def replace(group):
    mean, std = group.mean(), group.std()
    outliers = (group - mean).abs() > 3*std
    group[outliers] = mean        # or "group[~outliers].mean()"
    return group


def mean_confidence_interval(data, confidence=0.95):
    a = np.array(data)
    n = len(a)

    mean_ = np.mean(a, axis=0)
    std_ = np.std(a, axis=0)
    # h = se * scipy.stats.t.ppf((1 + confidence) / 2., n-1)

    # m = data
    # se = 1

    # m, se = np.mean(a, axis=0), scipy.stats.sem(a, axis=0)
    # h = se * scipy.stats.t.ppf((1 + confidence) / 2., n-1)
    return mean_, mean_-std_, mean_+std_


def parse(filestring, csv_file_name, optimizer_name):
    
    print(filestring)

    file_names = sorted(glob.glob(filestring))


    for file_name in file_names:
        print("file_name  ", file_name)

    losses = None
    errors = None


    min_index = 9e9
    for file_name in file_names: 
        df = pd.read_csv(file_name)
        indices = df['epoch']    

        print("indices ", indices.shape[0])

        if(indices.shape[0] < min_index):
            min_index = indices.shape[0]


    for file_name in file_names: 
        df = pd.read_csv(file_name)

        loss = df['loss_train'][0:min_index]
        error = df['L2_error_rel'][0:min_index]

        if(optimizer_name=="lbfgs"):
            indices = np.arange(0, min_index)
            g_evals = indices
        
        elif(optimizer_name=="aspqn"):
            indices = np.arange(0, min_index)
            g_evals = 77*indices # 5 sbds 
        
        elif(optimizer_name=="sl_aspqn"):
            indices = np.arange(0, min_index)
            g_evals = 52*indices # 5 sbds 
            # g_evals = 67*indices # 3 sbds 
            # g_evals = 52*indices # 2 sbds             

        if (errors is None):
            errors = error
            losses = loss
        else:
            errors = np.vstack((errors, error))
            losses = np.vstack((losses, loss))

    errors_mean, errors_minus, errors_plus = mean_confidence_interval(errors)
    loss_mean, loss_minus, loss_plus = mean_confidence_interval(losses)


    indices = indices.reshape(errors_mean.shape)
    g_evals = g_evals.reshape(errors_mean.shape)

    
    if(optimizer_name=="lbfgs"):
        memory_m = 3
        UC  = 4*memory_m*g_evals + g_evals

    elif(optimizer_name=="aspqn"):
        memory_m = 3
        num_sbd = 5
        num_layers = 6+4
        freq = 1
        overlap = 1
        num_local_steps = 50
        num_coarse_steps = 50

        # TODO:: verify 
        UC          = 4*memory_m*indices + indices # check frequency
        UC_local    = (num_layers/num_sbd+overlap)/num_layers*UC
        UC_coarse   = num_sbd/num_layers*UC

        UC          = 1/freq*(UC) + num_local_steps*(UC_local) + num_coarse_steps*UC_coarse

    elif(optimizer_name=="sl_aspqn"):
        memory_m = 3
        num_sbd = 5
        num_layers = 6+4
        freq = 1
        num_local_steps = 50

        # TODO:: verify 
        UC          = 4*memory_m*indices + indices # check frequency
        UC_local    = (num_layers/num_sbd)/num_layers*UC

        UC          = 1/freq*(4*memory_m*indices + 2*indices) + num_local_steps*(UC_local) 

    else:     
        print("Wrong choice")   
        exit(0)


    output = np.vstack((indices, g_evals, UC, errors_mean,  errors_minus,
                       errors_plus, loss_mean, loss_minus, loss_plus))
    output = output.T



    # subsampling in case of too many outputs 
    num_subs_entries = 300
    if(output.shape[0] > num_subs_entries):
        subs_indices = np.random.choice(min_index, int(num_subs_entries), replace=False)
        subs_indices.sort()
        output = output[subs_indices]


    pd.DataFrame(output).to_csv(csv_file_name, header  = ['epoch',  'g_evals', 'UC', 'errors_mean', 'errors_minus', 'errors_plus', 'loss_mean', 'loss_minus', 'loss_plus'])   



if __name__ == '__main__':

    # parse(filestring = 'BFGS__width_128_num_coarse_steps_0/*.csv', csv_file_name="DON_AniDiff_lbfgs.csv", optimizer_name="lbfgs")



    parse(filestring = 'SL_ASPQN__width_128_num_coarse_steps_0/*.csv', csv_file_name="sl_aspqn.csv", optimizer_name="sl_aspqn")




    # parse(filestring = 'TL_ASPQN__width_128_num_coarse_steps_50/*.csv', csv_file_name="TL_ASPQN.csv", optimizer_name="aspqn")


