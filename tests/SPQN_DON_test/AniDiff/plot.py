import pandas as pd
import time
import warnings
import scipy.stats
import numpy as np
import matplotlib.pyplot as plt
import glob
import argparse
import logging
import os
import sys
sys.setrecursionlimit(1000000)

# to ignore warnings ...
warnings.filterwarnings('ignore')

logging.disable(logging.WARNING)
os.environ["TF_CPP_MIN_LOG_LEVEL"] = "3"

# Just disables the warning, doesn't enable AVX/FMA
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'


def replace(group):
    mean, std = group.mean(), group.std()
    outliers = (group - mean).abs() > 3*std
    group[outliers] = mean        # or "group[~outliers].mean()"
    return group


def mean_confidence_interval(data, confidence=0.95):
    a = np.array(data)
    n = len(a)

    mean_ = np.mean(a, axis=0)
    std_ = np.std(a, axis=0)
    # h = se * scipy.stats.t.ppf((1 + confidence) / 2., n-1)

    # m = data
    # se = 1

    # m, se = np.mean(a, axis=0), scipy.stats.sem(a, axis=0)
    # h = se * scipy.stats.t.ppf((1 + confidence) / 2., n-1)
    return mean_, mean_-std_, mean_+std_


def ploting():
    
    filestring = 'BFGS__width_256_num_coarse_steps_25/Sum_seed_13_*.csv'

    print(filestring)

    file_names = sorted(glob.glob(filestring))

    # linestyles = ['-', ':', '-.']

    for file_name in file_names: 
        print("file_name  ", file_name)

        df = pd.read_csv(file_name)

        loss = df['loss_train']
        error = df['loss_val']
        g_evals = df['g_evals']
        indices = df['epoch']

        indices = indices.to_numpy()
        g_evals = g_evals.to_numpy()


        start_index = file_name.find("SPQN_num_coarse_steps_")
        end_index   = file_name.find("_num_subdomains_")
        cs          = file_name[start_index + len("SPQN_num_coarse_steps_"):end_index]


        start_index = file_name.find("num_subdomains_")
        end_index   = file_name.find("/Sum")
        num_sbds    = file_name[start_index + len("num_subdomains_"):end_index]


        col = 'b'
        plt.plot(indices, loss, color=col, linestyle='-', label=f'BFGS')        



    filestring = 'ASPQN__width_256_num_coarse_steps_25/Sum_seed_13_*num_local_steps_50*.csv'
    # filestring = 'ASPQN__width_256_num_coarse_steps_25/Sum_seed_13_*num_local_steps_25*.csv'

    print(filestring)

    file_names = sorted(glob.glob(filestring))

    # linestyles = ['-', ':', '-.']

    for file_name in file_names: 
        print("file_name  ", file_name)

        df = pd.read_csv(file_name)

        loss = df['loss_train']
        error = df['loss_val']
        g_evals = df['g_evals']
        indices = df['epoch']

        indices = indices.to_numpy()
        g_evals = g_evals.to_numpy()


        start_index = file_name.find("ASPQN__width_256_num_coarse_steps_")
        end_index   = file_name.find("/Sum_seed_")
        cs          = file_name[start_index + len("ASPQN__width_256_num_coarse_steps_"):end_index]


        start_index = file_name.find("num_subdomains_")
        end_index   = file_name.find("_width_256_hiddenlayers_")
        num_sbds    = file_name[start_index + len("num_subdomains_"):end_index]



        # if cs == "0":
        #     col = 'b'
        # elif cs == "5":
        #     col = 'r'
        # elif cs == "10":
        #     col = 'g'
        # elif cs == "25":
        #     col = 'purple'   
        # elif cs == "50":
        col = 'r'                                    

        print("cs ", cs)

        if(num_sbds =="3"):
            plt.plot(indices, loss, color=col, linestyle=':', label=f'cs_{cs}_sbd{num_sbds}')


        elif(num_sbds =="2"):
            plt.plot(indices, loss, color=col, linestyle='-', label=f'cs_{cs}_sbd{num_sbds}')
            

        elif(num_sbds =="6"):
            plt.plot(indices, loss, color=col, linestyle='--', label=f'cs_{cs}_sbd{num_sbds}')



    filestring = 'ASPQN__width_256_num_coarse_steps_50/Sum_seed_13_*num_local_steps_50*.csv'
    # filestring = 'ASPQN__width_256_num_coarse_steps_50/Sum_seed_13_*num_local_steps_25*.csv'
    print(filestring)

    file_names = sorted(glob.glob(filestring))

    # linestyles = ['-', ':', '-.']

    for file_name in file_names: 
        print("file_name  ", file_name)

        df = pd.read_csv(file_name)

        loss = df['loss_train']
        error = df['loss_val']
        g_evals = df['g_evals']
        indices = df['epoch']

        indices = indices.to_numpy()
        g_evals = g_evals.to_numpy()


        start_index = file_name.find("ASPQN__width_256_num_coarse_steps_")
        end_index   = file_name.find("/Sum_seed_")
        cs          = file_name[start_index + len("ASPQN__width_256_num_coarse_steps_"):end_index]


        start_index = file_name.find("num_subdomains_")
        end_index   = file_name.find("_width_256_hiddenlayers_")
        num_sbds    = file_name[start_index + len("num_subdomains_"):end_index]



        # if cs == "0":
        #     col = 'b'
        # elif cs == "5":
        #     col = 'r'
        # elif cs == "10":
        #     col = 'g'
        # elif cs == "25":
        #     col = 'purple'   
        # elif cs == "50":
        col = 'purple'                                    

        print("cs ", cs)

        if(num_sbds =="3"):
            plt.plot(indices, loss, color=col, linestyle=':', label=f'cs_{cs}_sbd{num_sbds}')


        elif(num_sbds =="2"):
            plt.plot(indices, loss, color=col, linestyle='-', label=f'cs_{cs}_sbd{num_sbds}')
            

        elif(num_sbds =="6"):
            plt.plot(indices, loss, color=col, linestyle='--', label=f'cs_{cs}_sbd{num_sbds}')
            


    plt.legend(loc='best', shadow=True)
    

    fileout = "test50_loss"
    png_file_name = fileout+".png"

    plt.ylabel('value')
    plt.xlabel('epochs')
    plt.yscale('log')
    # plt.xscale('log')
    plt.grid(True)
    
    plt.minorticks_on() # This is necessary to enable minor ticks
    plt.grid(which='minor')

    plt.title("#loc steps = 50")
    plt.ylim(1e-1, 6e-1)
    plt.xlim(0, 500)
    plt.savefig(png_file_name, dpi=150)




if __name__ == '__main__':

    ploting()



