#!/bin/bash
mkdir SL_BFGS

for seed in 125 1 546;
do
	sbatch --output=SL_BFGS/TL_ASM_seed${seed}_seed${seed}.out --job-name=SL_ASM submission_script_SL_ASPQN_daint.job $seed; 									
done
