#!/bin/bash
mkdir TL_ASM_new


for seed in 1 125 986;
do
	for num_points_x  in 25;
	do 	
		for num_points_y  in 50;
		do 			
			for num_ref in 4;
			do
				for ada_net in f;
				do				
					for max_epochs in 500000;
					do 
						for lr_global  in 1.0;
						do 				
							for lr_local  in 1.0;
							do 											
								for num_coarse_steps in 50;
								do 				
									for history in 3;
									do 		
										# for hiddenlayers_coarse in 2 4 10 18;
										for hiddenlayers_coarse in 10;
										do 												
											for num_local_steps in 50;
											do											
												for num_subdomains in $((($hiddenlayers_coarse+2)/2));
												do 			
													echo "layers: $hiddenlayers_coarse+2 and sbds: $num_subdomains"
													for width in 32;
													do 																							
														for ada_fun in t;
														do 		
															for num_levels in 1;
															do 	
																sbatch --output=TL_ASM_new/TL_ASM${lr_global}_lrloc${lr_local}_num_local_steps${num_local_steps}_seed${seed}_num_coarse_steps${num_coarse_steps}_history${history}_num_points_x${num_points_x}_num_points_y${num_points_y}_num_ref${num_ref}_ada_net${ada_net}_num_subdomains${num_subdomains}_max_epochs${max_epochs}_ada_fun${ada_fun}_numlayers_${hiddenlayers_coarse}_width${width}_num_levels${num_levels}.out --job-name=TL_ASM submission_script.job $seed $num_points_x $num_points_y $num_ref $ada_net $max_epochs $lr_global $lr_local $num_coarse_steps $history $num_subdomains $hiddenlayers_coarse $width $ada_fun $num_levels $num_local_steps; 
															done
														done
													done
												done
											done
										done
									done
								done
							done
						done
					done
				done
			done
		done
	done
done


