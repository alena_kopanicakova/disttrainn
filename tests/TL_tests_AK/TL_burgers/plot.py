import pandas as pd
import time
import warnings
import scipy.stats
import numpy as np
import matplotlib.pyplot as plt
import glob
import argparse
import logging
import os
import sys
sys.setrecursionlimit(1000000)

# to ignore warnings ...
warnings.filterwarnings('ignore')

logging.disable(logging.WARNING)
os.environ["TF_CPP_MIN_LOG_LEVEL"] = "3"

# Just disables the warning, doesn't enable AVX/FMA
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'


def replace(group):
    mean, std = group.mean(), group.std()
    outliers = (group - mean).abs() > 3*std
    group[outliers] = mean        # or "group[~outliers].mean()"
    return group


def mean_confidence_interval(data, confidence=0.95):
    a = np.array(data)
    n = len(a)

    mean_ = np.mean(a, axis=0)
    std_ = np.std(a, axis=0)
    # h = se * scipy.stats.t.ppf((1 + confidence) / 2., n-1)

    # m = data
    # se = 1

    # m, se = np.mean(a, axis=0), scipy.stats.sem(a, axis=0)
    # h = se * scipy.stats.t.ppf((1 + confidence) / 2., n-1)
    return mean_, mean_-std_, mean_+std_


def ploting(coarse_steps, num_subdomains):
    
    filestring = 'ASPQN_num_coarse_steps_*_num_subdomains_*/Sum_*.csv'

    print(filestring)

    file_names = sorted(glob.glob(filestring))


    # linestyles = ['-', ':', '-.']

    for file_name in file_names: 
        print("file_name  ", file_name)

        df = pd.read_csv(file_name)

        loss = df['loss_train']
        error = df['L2_error_rel']
        g_evals = df['g_evals']
        indices = df['epoch']

        indices = indices.to_numpy()
        g_evals = g_evals.to_numpy()


        start_index = file_name.find("SPQN_num_coarse_steps_")
        end_index   = file_name.find("_num_subdomains_")
        cs          = file_name[start_index + len("SPQN_num_coarse_steps_"):end_index]


        start_index = file_name.find("num_subdomains_")
        end_index   = file_name.find("/Sum")
        num_sbds    = file_name[start_index + len("num_subdomains_"):end_index]

        # for i in range(len(g_evals)):
        #     g_evals[i] += indices[i]*int(cs)

        if cs == "0":
            col = 'b'
        elif cs == "5":
            col = 'r'
        elif cs == "10":
            col = 'g'
        elif cs == "25":
            col = 'purple'   
        elif cs == "50":
            col = 'c'                                    


        if(num_sbds =="4"):
            # plt.plot(g_evals, error, color=col, linestyle=':', label=f'errors_{cs}_{num_sbds}')
            plt.plot(indices, error, color=col, linestyle='-', label=f'errors_{cs}_{num_sbds}')

            # plt.plot(indices, loss, color=col, linestyle=':', label=f'loss_{cs}_{num_sbds}')


        # elif(num_sbds =="4"):
        #     plt.plot(indices, error, color=col, linestyle=':', label=f'errors_{cs}_{num_sbds}')
        #     # plt.plot(indices, loss, color=col, linestyle=':', label=f'loss_{cs}_{num_sbds}')            

        # elif(num_sbds =="8"):
        #     plt.plot(indices, error, color=col, linestyle='-.', label=f'errors_{cs}_{num_sbds}')
        #     # plt.plot(indices, loss, color=col, linestyle='-.', label=f'loss_{cs}_{num_sbds}')                        



    filestring = 'FASNested_ASPQN_num_coarse_steps_*_num_subdomains_*/Sum_*.csv'

    print(filestring)

    file_names = sorted(glob.glob(filestring))


    # linestyles = ['-', ':', '-.']

    for file_name in file_names: 
        print("file_name  ", file_name)

        df = pd.read_csv(file_name)

        loss = df['loss_train']
        error = df['L2_error_rel']
        g_evals = df['g_evals']
        indices = df['epoch']

        indices = indices.to_numpy()
        g_evals = g_evals.to_numpy()


        start_index = file_name.find("SPQN_num_coarse_steps_")
        end_index   = file_name.find("_num_subdomains_")
        cs          = file_name[start_index + len("SPQN_num_coarse_steps_"):end_index]


        start_index = file_name.find("num_subdomains_")
        end_index   = file_name.find("/Sum")
        num_sbds    = file_name[start_index + len("num_subdomains_"):end_index]

        # for i in range(len(g_evals)):
        #     g_evals[i] += indices[i]*int(cs)

        if cs == "0":
            col = 'b'
        elif cs == "5":
            col = 'r'
        elif cs == "10":
            col = 'g'
        elif cs == "25":
            col = 'purple'   
        elif cs == "50":
            col = 'c'                                    


        if(num_sbds =="4" and cs != "10"):
            # plt.plot(g_evals, error, color=col, linestyle=':', label=f'errors_{cs}_{num_sbds}')
            plt.plot(indices, error, color=col, linestyle=':', alpha=1.0, label=f'FAS - LS - errors_{cs}_{num_sbds}')

            # plt.plot(indices, loss, color=col, linestyle=':', label=f'loss_{cs}_{num_sbds}')



        # elif(num_sbds =="4"):
        #     plt.plot(indices, error, color=col, linestyle=':', label=f'errors_{cs}_{num_sbds}')
        #     # plt.plot(indices, loss, color=col, linestyle=':', label=f'loss_{cs}_{num_sbds}')            

        # elif(num_sbds =="8"):
        #     plt.plot(indices, error, color=col, linestyle='-.', label=f'errors_{cs}_{num_sbds}')
        #     # plt.plot(indices, loss, color=col, linestyle='-.', label=f'loss_{cs}_{num_sbds}')                        



    filestring = 'CH_LS_ASPQN_num_coarse_steps_*_num_subdomains_*/Sum_*.csv'

    print(filestring)

    file_names = sorted(glob.glob(filestring))


    # linestyles = ['-', ':', '-.']

    for file_name in file_names: 
        print("file_name  ", file_name)

        df = pd.read_csv(file_name)

        loss = df['loss_train']
        error = df['L2_error_rel']
        g_evals = df['g_evals']
        indices = df['epoch']

        indices = indices.to_numpy()
        g_evals = g_evals.to_numpy()


        start_index = file_name.find("SPQN_num_coarse_steps_")
        end_index   = file_name.find("_num_subdomains_")
        cs          = file_name[start_index + len("SPQN_num_coarse_steps_"):end_index]


        start_index = file_name.find("num_subdomains_")
        end_index   = file_name.find("/Sum")
        num_sbds    = file_name[start_index + len("num_subdomains_"):end_index]

        # for i in range(len(g_evals)):
        #     g_evals[i] += indices[i]*int(cs)

        if cs == "0":
            col = 'b'
        elif cs == "5":
            col = 'r'
        elif cs == "10":
            col = 'g'
        elif cs == "25":
            col = 'purple'   
        elif cs == "50":
            col = 'c'                                    


        if(num_sbds =="4"):
            # plt.plot(g_evals, error, color=col, linestyle=':', label=f'errors_{cs}_{num_sbds}')
            plt.plot(indices, error, color=col, linestyle='-', alpha=0.2, label=f'CH -LS -  errors_{cs}_{num_sbds}')

            # plt.plot(indices, loss, color=col, linestyle=':', label=f'loss_{cs}_{num_sbds}')





    plt.legend(loc='best', shadow=True)
    
    
    fileout = "C-all4"
    png_file_name = fileout+".png"

    plt.ylabel('value')
    plt.xlabel('epochs')
    plt.yscale('log')
    # plt.xscale('log')
    plt.grid(True)
    # plt.xlim(0,5000)

    plt.title(fileout)
    plt.ylim(1e-5, 1)
    plt.savefig(png_file_name, dpi=150)



if __name__ == '__main__':

    # coarse_steps = [0, 5, 10, 25]
    # num_subdomains = [2, 4, 8]
    # for i_locsteps in coarse_steps:
    #     for j_sd in num_subdomains:
    #         plt.figure(j_sd*i_locsteps)



    coarse_steps    = [0, 5, 10, 25]
    num_subdomains  = [2, 4, 8]

    ploting(coarse_steps, num_subdomains)



