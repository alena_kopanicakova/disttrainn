import numpy as np
import scipy.io as io
import os
import matplotlib.pyplot as plt
import time
import scipy.io as io
import sys
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.gridspec as gridspec
import copy
import torch.optim as optim
import argparse
import pandas as pd
from pathlib import Path

from models.FFNConstantWidth import *
from datasets.Domain2D import *
from PDEs.AllenCahn import *
from models.ResNetDenseConstantWidth import *
from trainers.SPQNTrainerRight import *
from trainers.PrecLBFGS import *


# python3 test_SPQN_twolevel_additive_serial.py  --num_points_x 25 --num_points_y 25 --num_ref 2 --num_subdomains 6 --lr_global 1  --hiddenlayers_coarse 4 --lr_local 1.0 --max_epochs 1000 --use_adaptive_activation True --num_levels 1 world_size  1  args.num_subdomains  6


args = get_params()
torch.set_default_dtype(torch.float64)

world_size = 1
rank = 0

print("world_size ", world_size, " args.num_subdomains ", args.num_subdomains)
print(" \n \n args ", args, " \n \n ")

print(" - - - BFGS  - level test  - - -")
args.use_SPQN = False



np.random.seed(args.seed)
torch.manual_seed(args.seed)
torch.cuda.manual_seed(args.seed)
np.random.seed(args.seed)

pi = torch.acos(torch.zeros(1)).item() * 2

train_samples = np.array([args.num_ref*args.num_points_x, args.num_ref*args.num_points_y])
test_samples = np.array([200, 400])

start_point = np.array([0., -1.0]) # (t_0, x_0)
end_point = np.array([1.0, 1.]) # (t_end, x_end)

domain = RectangleDomain2D( start_point,
                            end_point,
                            train_samples,
                            test_samples,
                            use_BC=False,
                            sampling_strategy="hammersly", # uniform_random, hammersly
                            ref_strategy="None")


bc  = BC_AllenCahn(start_point=start_point, end_point=end_point)
pde = AllenCahn(bc_exact=bc)

net = ResNetDenseConstantWidth( inputs=2,\
                                outputs=1,\
                                hiddenlayers=args.hiddenlayers_coarse,\
                                width=args.width,\
                                dt=args.T/(args.hiddenlayers_coarse-1),\
                                use_adaptive_activation=True)



domain.append_analytical_sol_test(pde)
net.print_decomposition(args.num_subdomains)


elapsed_time = 0
epoch = 0
train_loss = 0
converged = False


dir_name = "BFGS_"+"num_coarse_steps_"+str(args.num_coarse_steps)+"_num_subdomains_"+str(args.num_subdomains)
Path(dir_name).mkdir(parents=True, exist_ok=True)


trainer = ASPQNTrainerRight(training_pinn_flg=True,
                            num_subdomains=args.num_subdomains,
                            type="regression",
                            freq_print=1,
                            use_coarse=args.use_coarse, 
                            layerwise_lr_update=args.use_layerwise_lr)


trainer.config["max_epochs"] = args.max_epochs
trainer.config["conv_history_csv_name"] = dir_name + "/" + \
                                          'Sum_seed_' + str(args.seed)+\
                                          "_num_coarse_steps_" + str(args.num_coarse_steps) +\
                                          '_lr_global_'+str(args.lr_global) +\
                                          '_lrl_local_'+str(args.lr_local) +\
                                          '_num_local_steps_'+str(args.num_local_steps)+\
                                          "_max_epochs_" + str(args.max_epochs) +\
                                          "_num_points_x_"+str(args.num_points_x)+\
                                          "_num_points_y_"+str(args.num_points_y)+\
                                          "_num_ref_"+str(args.num_ref)+\
                                          "_num_subdomains_"+str(args.num_subdomains)+\
                                          "_width_"+str(args.width)+\
                                          "_hiddenlayers_coarse_"+str(args.hiddenlayers_coarse)+\
                                          "_ada_net_"+str(args.ada_net)+\
                                          "_use_SPQN_"+str(args.use_SPQN)+\
                                          "_freq_"+str(args.freq)+\
                                          "_use_adaptive_activation_"+str(args.use_adaptive_activation)


start_time = time.time()

convergence_status = trainer.train( dataset=domain,
                                    net=net,
                                    criterion=pde.criterion,
                                    global_optimizer_type=PrecLBFGS,
                                    local_optimizer_type=PrecLBFGS,
                                    args=args)


elapsed_time = time.time() - start_time

out_name = dir_name + "/" + 'Summary.csv'

df = pd.DataFrame({'seed': [args.seed],
                 'num_coarse_steps': [args.num_coarse_steps],
                 'num_points_x':[args.num_points_x],
                 'num_points_y':[args.num_points_y],
                 'num_ref':[args.num_ref],
                 'num_levels': [args.num_levels],
                 'width': [args.width],
                 'hiddenlayers_coarse': [args.hiddenlayers_coarse],
                 'num_subdomains': [args.num_subdomains],
                 'num_local_steps':[args.num_local_steps],
                 'ada_net': [args.ada_net],
                 'use_adaptive_activation':[args.use_adaptive_activation],
                 'use_SPQN': [args.use_SPQN],
                 'freq':[args.freq],
                 'T': [args.T],
                 'lr_global': [args.lr_global],
                 'lr_local': [args.lr_local],
                 'history': [args.history],
                 'time': [elapsed_time],
                 'epochs': [convergence_status["epochs"]],
                 'l_evals': [convergence_status["num_loss_evals"]],
                 'g_evals': [convergence_status["num_grad_evals"]],
                 'loss': [convergence_status["loss_train"]],
                 'loss_test': [convergence_status["loss_val"]],
                 'L2_error': [convergence_status["L2_error"]],
                 'L2_error_rel': [convergence_status["L2_error_rel"]],
                 'overlap_width': [args.overlap_width]})

if Path(out_name).exists():
    df.to_csv(out_name, index=False, header=False, mode='a')
else:
    df.to_csv(out_name, index=False, header=True, mode='a')


pde.plot_results(domain, net, dpi=1000)

