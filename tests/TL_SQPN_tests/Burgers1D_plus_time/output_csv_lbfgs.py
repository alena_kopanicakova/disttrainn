import pandas as pd
import time
import warnings
import scipy.stats
import numpy as np
import matplotlib.pyplot as plt
import glob
import argparse
import logging
import os
import sys
sys.setrecursionlimit(1000000)

# to ignore warnings ...
warnings.filterwarnings('ignore')

logging.disable(logging.WARNING)
os.environ["TF_CPP_MIN_LOG_LEVEL"] = "3"

# Just disables the warning, doesn't enable AVX/FMA
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'


def replace(group):
    mean, std = group.mean(), group.std()
    outliers = (group - mean).abs() > 3*std
    group[outliers] = mean        # or "group[~outliers].mean()"
    return group


def mean_confidence_interval(data, confidence=0.95):
    a = np.array(data)
    n = len(a)

    mean_ = np.mean(a, axis=0)
    std_ = np.std(a, axis=0)


    return mean_, mean_-std_, mean_+std_


def ploting():
    filestring = 'Algo_1/Summary_seed_*_lrg1.0_opt_type_1max_epochs100000_num_points_x_25_num_points_y_50_num_ref_4_num_levels_1_width_20_hiddenlayers_coarse_6_ada_net_False_T_6.0_history_3_use_adaptive_activation_True.csv'
    print(filestring)

    file_names = glob.glob(filestring)

    for file_name in file_names:
        print("file_name  ", file_name)

    losses = None
    errors = None



    for file_name in file_names:

        df = pd.read_csv(file_name)

        loss = df['loss_train']
        error = df['L2_error_rel']
        g_evals = df['epoch']
        indices = df['epoch']

        if (errors is None):
            errors = error
            losses = loss
        else:
            errors = np.vstack((errors, error))
            losses = np.vstack((losses, loss))

    errors_mean, errors_minus, errors_plus = mean_confidence_interval(
        errors)
    loss_mean, loss_minus, loss_plus = mean_confidence_interval(losses)

    indices = indices.to_numpy()
    indices = indices.reshape(errors_mean.shape)

    g_evals = g_evals.to_numpy()
    g_evals = g_evals.reshape(errors_mean.shape)

    
    memory_m = 3
    UC  = 4*memory_m*g_evals + g_evals


    output = np.vstack((indices, g_evals, UC, errors_mean,  errors_minus,
                       errors_plus, loss_mean, loss_minus, loss_plus))
    output = output.T



    # print("output ", output)
    # # print("g_evals ", g_evals.shape)

    # # print("loss_mean ", loss_mean.shape)
    # # print("errors_mean ", errors_mean.shape)


    csv_file_name="lbfgs_burgers.csv"
    pd.DataFrame(output).to_csv(csv_file_name, header  = ['epoch',  'g_evals', 'UC', 'errors_mean', 'errors_minus', 'errors_plus', 'loss_mean', 'loss_minus', 'loss_plus'])   


    exit(0)



if __name__ == '__main__':

    # file_names = glob.glob('MSPQN_1/Summary_seed_*_lrl_local_0.005_num_local_steps_10max_epochs1000_*ref_4*_num_subdomains_4_*.csv')
    # num_local_steps = [10, 50, 100]
    # num_subdomains = [2, 3, 6]
    # for i_locsteps in num_local_steps:
    #     for j_sd in num_subdomains:
    #         plt.figure(j_sd*i_locsteps)
    ploting()

            


