#!/bin/bash

export PYTHONPATH="${PYTHONPATH}:/users/ylee285/scratch/disttrainn"
export PYTHONPATH="${PYTHONPATH}:/Users/lua/research/disttrainn"

# for seed in 13;
# for seed in 1 13;
# for seed in 1 13 125 546;
for seed in 1;
do
	for num_points_x  in 25;
	do 	
		for num_points_y  in 50;
		do 			
			for num_ref in 4;
			# for num_ref in 1 2 3;
			do
				for ada_net in f;
				do				
					for max_epochs in 200;
					# for max_epochs in 100000;
					do 
						for lr_global  in 1.0;
						do 				
							# for lr_local  in 5e-2 5e-3 1e-3 5e-4 1e-4;
							for lr_local  in 1.0;
							do 											
								for opt_type in 1;
								do 				
									for history in 3;
									do 		
										for num_subdomains in 4;
										# for num_subdomains in 8;
										do 	
											# for num_local_steps in 10 50 100;									
											for num_local_steps in 50;
											do														
												for hiddenlayers_coarse in 6;
												do 		
													for width in 20;
													do 												
														for T in 6;
														do 												
															for ada_fun in t;
															do 		
																for num_levels in 1;
																do 										
																	for overlap_width in 0 1;
																	# for overlap_width in 0;
																	do
																		for use_layerwise_lr in True;
																		do
																			for num_coarse_steps in 25;
																			do
																				for use_stochastic in True;
																				do
																					for freq in 5 25 50 100 200;
																					do
																						python test_SPQN_twolevel_additive_serial.py --seed ${seed} --num_points_x ${num_points_x} --num_points_y ${num_points_y} --num_ref ${num_ref} --ada_net ${ada_net} --max_epochs ${max_epochs} --lr_global ${lr_global} --lr_local ${lr_local} --opt_type ${opt_type} --history ${history} --num_subdomains ${num_subdomains} --hiddenlayers_coarse ${hiddenlayers_coarse} --width ${width} --T ${T} --ada_fun ${ada_fun} --num_levels ${num_levels} --num_local_steps ${num_local_steps} --overlap_width ${overlap_width} --use_layerwise_lr ${use_layerwise_lr} --num_coarse_steps ${num_coarse_steps} --use_stochastic ${use_stochastic} --freq ${freq}
																					done
																				done
																			done
																		done
																	done
																done
															done
														done
													done
												done
											done
										done
									done
								done
							done
						done
					done
				done
			done
		done
	done
done