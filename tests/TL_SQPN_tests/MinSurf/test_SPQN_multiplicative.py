import numpy as np
import scipy.io as io
import os
import matplotlib.pyplot as plt
import time
import scipy.io as io
import sys
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.gridspec as gridspec
import copy
import torch.optim as optim
import argparse
import pandas as pd
from pathlib import Path

from models.FFNConstantWidth import *
from datasets.Domain2D import *
from PDEs.MinSurf2D import *
from models.ResNetDenseConstantWidth import *
from trainers.SPQNTrainerRight import *
from trainers.PrecLBFGS import *
from trainers.GDLinesearch import *

args = get_params()
torch.set_default_dtype(torch.float64)

world_size = 1
rank = 0

print("world_size ", world_size, " args.num_subdomains ", args.num_subdomains)
print(" \n \n args ", args, " \n \n ")

print(" - - - MULTIPLICATIVE SPQN  - - -")

np.random.seed(args.seed)
torch.manual_seed(args.seed)
torch.cuda.manual_seed(args.seed)
np.random.seed(args.seed)

pi = torch.acos(torch.zeros(1)).item() * 2

train_samples = np.array([args.num_ref*args.num_points_x, args.num_ref*args.num_points_y])
test_samples = np.array([200, 400])

start_point = np.array([0., 0.])
end_point = np.array([1., 1.])

domain = RectangleDomain2D( start_point,
                            end_point,
                            train_samples,
                            test_samples,
                            use_BC=False,
                            sampling_strategy="hammersly", # uniform_random, hammersly
                            ref_strategy="None")


bc_exact  = BC_MinSurf2D(start_point=start_point, end_point=end_point)
pde       = MinSurf2D(bc_exact=bc_exact)

ml_hierarchy = MLHierarchyResNets(num_levels=args.num_levels)
nets = ml_hierarchy.build(inputs=2, 
                          outputs=1,
                          width=args.width,
                          hiddenlayers_coarse=args.hiddenlayers_coarse,
                          T=args.T,
                          use_adaptive_activation=args.use_adaptive_activation)


domain.append_analytical_sol_test(pde)
nets[-1].print_decomposition(args.num_subdomains)


elapsed_time = 0
epoch = 0
train_loss = 0
converged = False

if(args.opt_type == 1):
    algo = PrecLBFGS
    if(args.lr_global < 1.0):
      exit(0)
elif(args.opt_type == 2):
    algo = optim.Adam
else:
    print("Inner algo type not supported. ")



dir_name = "MSPQN_"+"opt_type_"+str(args.opt_type)+"_overlap_"+str(args.overlap_width)
Path(dir_name).mkdir(parents=True, exist_ok=True)


trainer = MSPQNTrainerRight(training_pinn_flg=True,
                            num_subdomains=args.num_subdomains,
                            type="regression",
                            freq_print=1,
                            use_wandb=False)


trainer.config["max_epochs"] = args.max_epochs
trainer.config["conv_history_csv_name"] = dir_name + "/" + \
                                          'Sum_seed_' + str(args.seed)+\
                                          "_opt_type_" + str(args.opt_type) +\
                                          '_lr_global_'+str(args.lr_global) +\
                                          '_lrl_local_'+str(args.lr_local) +\
                                          '_num_local_steps_'+str(args.num_local_steps)+\
                                          "_max_epochs_" + str(args.max_epochs) +\
                                          "_num_points_x_"+str(args.num_points_x)+\
                                          "_num_points_y_"+str(args.num_points_y)+\
                                          "_num_ref_"+str(args.num_ref)+\
                                          "_num_levels_"+str(args.num_levels)+\
                                          "_num_subdomains_"+str(args.num_subdomains)+\
                                          "_ada_net_"+str(args.ada_net)+\
                                          "_T_"+str(args.T)+\
                                          "_history_"+str(args.history)+\
                                          "_use_adaptive_activation_"+str(args.use_adaptive_activation)
    

start_time = time.time()
if(args.ada_net==False):
  convergence_status = trainer.train(dataset=domain,
                                      net=nets[-1],
                                      criterion=pde.criterion,
                                      global_optimizer_type=PrecLBFGS,\
                                      local_optimizer_type=algo,\
                                      args=args)




  elapsed_time = time.time() - start_time

  # if(rank==0):
  out_name = dir_name + "/" + 'Summary.csv'

  df = pd.DataFrame({'seed': [args.seed],
                     'opt_type': [args.opt_type],
                     'num_points_x':[args.num_points_x],
                     'num_points_y':[args.num_points_y],
                     'num_ref':[args.num_ref],
                     'num_levels': [args.num_levels],
                     'num_subdomains': [args.num_subdomains],
                     'num_local_steps':[args.num_local_steps],
                     'ada_net': [args.ada_net], 
                     'use_adaptive_activation':[args.use_adaptive_activation],
                     'T': [args.T], 
                     'lr_global': [args.lr_global],
                     'lr_local': [args.lr_local],
                     'history': [args.history],
                     'time': [elapsed_time],
                     'epochs': [convergence_status["epochs"]],
                     'l_evals': [convergence_status["num_loss_evals"]],
                     'g_evals': [convergence_status["num_grad_evals"]],
                     'loss': [convergence_status["loss_train"]],
                     'loss_test': [convergence_status["loss_val"]],
                     'L2_error': [convergence_status["L2_error"]],
                     'L2_error_rel': [convergence_status["L2_error_rel"]]})

else:

  convergence_statuses = []

  

  for l in range(0, ml_hierarchy.num_levels): 
    print("------------  level l ", l, " ------------")

    if(l > 0):
      print("l-1: ", l-1, "l:  ", l)
      ml_hierarchy.prolong_params(nets[l-1], nets[l])


    ml_scaling_factor =  (2**((l+1)- args.num_levels))
    trainer.config["max_epochs"] = int((1./ml_scaling_factor)*(args.max_epochs/ml_hierarchy.num_levels))


    convergence_status = trainer.train(dataset=domain,
                                        net=nets[-1],
                                        criterion=pde.criterion,
                                        global_optimizer_type=PrecLBFGS,\
                                        local_optimizer_type=optim.Adam,\
                                        args=args)


    convergence_statuses.append(convergence_status)


  elapsed_time = time.time() - start_time
  epochs=0
  num_loss_evals=0
  num_grad_evals=0

  for l in range(0, len(convergence_statuses)): 
    
    ml_scaling_factor =  (2**((l+1)- args.num_levels))
    print("l ", l, "   ml_scaling_factor ", ml_scaling_factor)

    epochs          += ml_scaling_factor * convergence_statuses[l]["epochs"]
    num_loss_evals  += ml_scaling_factor * convergence_statuses[l]["num_loss_evals"]
    num_grad_evals  += ml_scaling_factor * convergence_statuses[l]["num_grad_evals"]


  out_name = dir_name + "/" + 'Summary.csv'


  df = pd.DataFrame({'seed': [args.seed],
                     'opt_type': [args.opt_type],
                     'num_points_x':[args.num_points_x],
                     'num_points_y':[args.num_points_y],
                     'num_ref':[args.num_ref],
                     'num_levels': [args.num_levels],
                     'num_subdomains': [args.num_subdomains],
                     'num_local_steps':[args.num_local_steps],
                     'ada_net': [args.ada_net], 
                     'use_adaptive_activation':[args.use_adaptive_activation],
                     'T': [args.T], 
                     'lr_global': [args.lr_global],
                     'lr_local': [args.lr_local],
                     'history': [args.history],
                     'time': [elapsed_time],
                     'epochs': [epochs],
                     'l_evals': [num_loss_evals],
                     'g_evals': [num_grad_evals],                     
                     'loss': [convergence_status["loss_train"]],
                     'loss_test': [convergence_status["loss_val"]],
                     'L2_error': [convergence_status["L2_error"]],
                     'L2_error_rel': [convergence_status["L2_error_rel"]]})



if Path(out_name).exists():
    df.to_csv(out_name, index=False, header=False, mode='a')
else:
    df.to_csv(out_name, index=False, header=True, mode='a')


pde.plot_results(domain, nets[-1], dpi=1000)

