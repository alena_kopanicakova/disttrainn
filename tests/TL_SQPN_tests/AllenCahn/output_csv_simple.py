import pandas as pd
import time
import warnings
import scipy.stats
import numpy as np
import matplotlib.pyplot as plt
import glob
import argparse
import logging
import os
import sys
sys.setrecursionlimit(1000000)

# to ignore warnings ...
warnings.filterwarnings('ignore')

logging.disable(logging.WARNING)
os.environ["TF_CPP_MIN_LOG_LEVEL"] = "3"

# Just disables the warning, doesn't enable AVX/FMA
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'


def replace(group):
    mean, std = group.mean(), group.std()
    outliers = (group - mean).abs() > 3*std
    group[outliers] = mean        # or "group[~outliers].mean()"
    return group


def mean_confidence_interval(data, confidence=0.95):
    a = np.array(data)
    n = len(a)

    mean_ = np.mean(a, axis=0)
    std_ = np.std(a, axis=0)
    # h = se * scipy.stats.t.ppf((1 + confidence) / 2., n-1)

    # m = data
    # se = 1

    # m, se = np.mean(a, axis=0), scipy.stats.sem(a, axis=0)
    # h = se * scipy.stats.t.ppf((1 + confidence) / 2., n-1)
    return mean_, mean_-std_, mean_+std_


def ploting_for_sd_numsteps(i_locsteps, j_sd):
    types = 'A'
    overlaps = list(range(3))
    add_coarse = False
    folder = 'SPQN_opt_type_1_overlap_?'
    if add_coarse:
        folder += '_coarse_delta_*'
    filestring = 'results/' + types + \
        folder + '/Summary_seed_*_opt_type_1_lr_global_1.0_lrl_local_1.0_num_local_steps_' + \
        str(i_locsteps)+'_max_epochs_*_num_points_x_25_num_points_y_50_num_ref_4_num_levels_1_num_subdomains_' + \
        str(j_sd) + \
        '_*.csv'
    print(filestring)

    file_names = sorted(glob.glob(filestring))

    linestyles = ['-', ':', '-.']

    for i in range(len(overlaps)):
        linestyle = linestyles[i]
        k_ov = overlaps[i]
        file_name = file_names[i]
        print("file_name  ", file_name)

        df = pd.read_csv(file_name)

        loss = df['loss_train']
        error = df['L2_error_rel']
        g_evals = df['g_evals']
        indices = df['epoch']

        indices = indices.to_numpy()

        g_evals = g_evals.to_numpy()

        plt.plot(indices, error, color='b', linestyle=linestyle, label=f'errors_overlap_{k_ov}')
        plt.plot(indices, loss, color='r', linestyle=linestyle, label=f'loss_overlap_{k_ov}')

    plt.legend(loc='lower left', shadow=True)
    fileout = types +\
        'SPQN_bfgs_AllenCahn_subdomains_' + \
        str(j_sd)+'_num_local_steps_'+str(i_locsteps)
    if add_coarse:
        fileout += '_add_coarse'
    png_file_name = fileout+".png"

    plt.ylabel('value')
    plt.xlabel('epochs')
    plt.yscale('log')
    # plt.xscale('log')
    plt.grid(True)
    plt.title(fileout)
    plt.ylim(1e-7, 5)
    plt.savefig(png_file_name)


if __name__ == '__main__':

    num_local_steps = [50]
    num_subdomains = [2, 4, 8, 16]
    for i_locsteps in num_local_steps:
        for j_sd in num_subdomains:
            plt.figure(j_sd*i_locsteps)
            ploting_for_sd_numsteps(i_locsteps=i_locsteps, j_sd=j_sd)
