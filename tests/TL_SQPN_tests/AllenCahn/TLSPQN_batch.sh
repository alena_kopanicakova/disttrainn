#!/bin/bash

mkdir ASPQN
# mkdir MSPQN

# for seed in 13;
# for seed in 1 13;
for seed in 1 13 125 546 722;
# for seed in 125 546 1 13;
do
	for num_points_x  in 25;
	do 	
		for num_points_y  in 50;
		do 			
			for num_ref in 4;
			# for num_ref in 1 2 3;
			do
				for ada_net in f;
				do				
					# for max_epochs in 200;
					for max_epochs in 200000;
					do 
						for lr_global  in 1.0;
						do 				
							# for lr_local  in 5e-2 5e-3 1e-3 5e-4 1e-4;
							for lr_local  in 1.0;
							do 											
								for opt_type in 1;
								do 				
									for history in 3;
									do 		
										for num_subdomains in 2 3;
										# for num_subdomains in 6;
										do 	
											# for num_local_steps in 10 50 100;									
											for num_local_steps in 50;
											do														
												for hiddenlayers_coarse in 4;
												do 		
													for width in 64;
													do 												
														for T in 4;
														do 												
															for ada_fun in t;
															do 		
																for num_levels in 1;
																do 										
																	for overlap_width in 1;
																	# for overlap_width in 0 1 2;
																	do
																		for use_layerwise_lr in True;
																		do
																			for num_coarse_steps in 50;
																			do
																				sbatch --output=ASPQN/ASPQN${lr_global}_lrloc${lr_local}_num_local_steps${num_local_steps}_seed${seed}_opt_type${opt_type}_history${history}_num_points_x${num_points_x}_num_points_y${num_points_y}_num_ref${num_ref}_ada_net${ada_net}_num_subdomains${num_subdomains}_max_epochs${max_epochs}_T${T}_ada_fun${ada_fun}_numlayers_${hiddenlayers_coarse}_width${width}_num_levels${num_levels}_overlap${overlap_width}_coarse_step_${num_coarse_steps}.out --job-name=ASPQN submission_script_TLASPQN_oscar.job $seed $num_points_x $num_points_y $num_ref $ada_net $max_epochs $lr_global $lr_local $opt_type $history $num_subdomains $hiddenlayers_coarse $width $T $ada_fun $num_levels $num_local_steps $overlap_width $use_layerwise_lr $num_coarse_steps;
																			done
																		done
																	done
																done
															done
														done
													done
												done
											done
										done
									done
								done
							done
						done
					done
				done
			done
		done
	done
done