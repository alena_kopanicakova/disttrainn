#!/bin/bash

mkdir LBFGS_out

for seed in 1 13 123 567;
do
	for num_points_x  in 25;
	do 	
		for num_points_y  in 50;
		do 			
			for num_ref in 4;
			do
				for ada_net in f;
				do				
					for max_epochs in 30000;
					do 
						for lr_global  in 1.0;
						do 				
							for opt_type in 1;
							do 				
								for history in 3;
								do 		
									for hiddenlayers_coarse in 4;
									do 		
										for width in 50;
										do 												
											for T in 4;
											do 										
												for ada_fun in t;
												do 		
													for num_levels in 1;
													do 																																				
														sbatch --output=LBFGS_out/LBFGS${lr_global}_seed${seed}_opt_type${opt_type}_history${history}_num_points_x${num_points_x}_num_points_y${num_points_y}_num_ref${num_ref}_ada_net${ada_net}_max_epochs${max_epochs}_T${T}_ada_fun${ada_fun}_numlayers_${hiddenlayers_coarse}_width${width}_num_levels${num_levels}.out  --job-name=LBFGS submission_script_serial_daint.job $seed $num_points_x $num_points_y $num_ref $ada_net $max_epochs $lr_global $opt_type $history $hiddenlayers_coarse $width $T $ada_fun $num_levels; 
													done
												done
											done
										done
									done
								done
							done
						done
					done
				done
			done
		done
	done
done