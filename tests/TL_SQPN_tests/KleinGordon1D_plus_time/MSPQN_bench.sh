#!/bin/bash

mkdir MSPQN_out

for seed in 138 135;
do
	for num_points_x  in 25;
	do 	
		for num_points_y  in 50;
		do 			
			for num_ref in 4;
			do
				for ada_net in f;
				do				
					for max_epochs in 30000;
					do 
						for lr_global  in 1.0;
						do 				
							for lr_local  in 1.0;
							do 											
								for opt_type in 1;
								do 				
									for history in 3;
									do 		
										for num_subdomains in 2 3 6;
										do 					
											for num_local_steps in 10 50 100;
											do																			
												for hiddenlayers_coarse in 4;
												do 		
													for width in 25 50;
													do 												
														for T in 4;
														do 														
															for ada_fun in t;
															do 		
																for num_levels in 1;
																do 																																				
																	sbatch --output=MSPQN_out/MSPQN${lr_global}_lrloc${lr_local}_num_local_steps${num_local_steps}_seed${seed}_opt_type${opt_type}_history${history}_num_points_x${num_points_x}_num_points_y${num_points_y}_num_ref${num_ref}_ada_net${ada_net}_num_subdomains${num_subdomains}_max_epochs${max_epochs}_T${T}_ada_fun${ada_fun}_numlayers_${hiddenlayers_coarse}_width${width}_num_levels${num_levels}.out --job-name=MSPQN submission_script_MSPQN.job $seed $num_points_x $num_points_y $num_ref $ada_net $max_epochs $lr_global $lr_local $opt_type $history $num_subdomains $hiddenlayers_coarse $width $T $ada_fun $num_levels $num_local_steps; 
																done
															done
														done
													done
												done
											done
										done
									done
								done
							done
						done
					done
				done
			done
		done
	done
done
