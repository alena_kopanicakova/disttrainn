
import sys
sys.setrecursionlimit(1000000)
import os
import logging
import argparse
import glob
import matplotlib.pyplot as plt

# to ignore warnings ...
import warnings
warnings.filterwarnings('ignore')

logging.disable(logging.WARNING)
os.environ["TF_CPP_MIN_LOG_LEVEL"] = "3"

# Just disables the warning, doesn't enable AVX/FMA
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'

import time
import pandas as pd


import time

def replace(group):
    mean, std = group.mean(), group.std()
    outliers = (group - mean).abs() > 3*std
    group[outliers] = mean        # or "group[~outliers].mean()"
    return group


if __name__ == '__main__':

    file_names = glob.glob('MSPQN_1/Summary.csv')

    pd.set_option('display.max_columns', None)  # or 1000
    pd.set_option('display.max_rows', None)  # or 1000
    pd.set_option('display.max_colwidth', None)  # or 199



    for file_name in file_names:

    	df = pd.read_csv (file_name)
    	# df = df.loc[df["wf"]=="adagrad"]

    	h_app = df.groupby(['num_ref', 'lr_local','num_local_steps', 'num_subdomains'])

    	h_app_mean = h_app['L2_error_rel'].mean()
    	h_app_mean.name = "mean"


    	h_app_std = h_app['L2_error_rel']
    	h_app_std = h_app_std.std(ddof=0)
    	h_app_std.name = "std"


    	frames = [h_app_mean, h_app_std]
    	results = pd.concat(frames, axis=1)


    	print(results)





