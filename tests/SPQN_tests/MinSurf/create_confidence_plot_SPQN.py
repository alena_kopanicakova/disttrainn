import sys
sys.setrecursionlimit(1000000)
import os
import logging
import argparse
import glob
import matplotlib.pyplot as plt
import numpy as np
import scipy.stats

# to ignore warnings ...
import warnings
warnings.filterwarnings('ignore')

logging.disable(logging.WARNING)
os.environ["TF_CPP_MIN_LOG_LEVEL"] = "3"

# Just disables the warning, doesn't enable AVX/FMA
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'

import time
import pandas as pd
import time


def replace(group):
    mean, std = group.mean(), group.std()
    outliers = (group - mean).abs() > 3*std
    group[outliers] = mean        # or "group[~outliers].mean()"
    return group


def mean_confidence_interval(data, confidence=0.95):
    a = np.array(data)
    n = len(a)

    m = data
    se = 1


    # m, se = np.mean(a, axis=0), scipy.stats.sem(a, axis=0)
    h = se * scipy.stats.t.ppf((1 + confidence) / 2., n-1)
    return m, m-h, m+h


if __name__ == '__main__':

    # file_names = glob.glob('MSPQN_1/Summary_seed_*_lrl_local_0.005_num_local_steps_10max_epochs1000_*ref_4*_num_subdomains_4_*.csv')   
    file_names = glob.glob('MSPQN_1/Summary_seed_13_opt_type_2_lr_global_1.0_lrl_local_0.001_num_local_steps_10max_epochs1000_num_points_x_25_num_points_y_50_num_ref_4_num_levels_1_num_subdomains_2_ada_net_False_T_4.0_history_3_use_adaptive_activation_True.csv')   


    # pd.set_option('display.max_columns', None)  # or 1000
    # pd.set_option('display.max_rows', None)  # or 1000
    # pd.set_option('display.max_colwidth', None)  # or 199

    for file_name in file_names:
        print("file_name  ", file_name)


    losses=None
    errors=None

    for file_name in file_names:

    	df = pd.read_csv (file_name)

    	loss   = df['loss_train']
    	error  = df['L2_error_rel']
    	g_evals = df['g_evals']
    	indices = df['epoch']


    	if(errors is None):
    		errors = error
    		losses = loss
    	else:
    		errors = np.vstack((errors, error))
    		losses = np.vstack((losses, loss))


    

    errors_mean, errors_minus, errors_plus = mean_confidence_interval(errors)
    loss_mean, loss_minus, loss_plus = mean_confidence_interval(losses)


    indices = indices.to_numpy()
    indices = indices.reshape(errors_mean.shape)

    g_evals = g_evals.to_numpy()
    g_evals = g_evals.reshape(errors_mean.shape)    


    output = np.vstack((indices, g_evals, errors_mean,  errors_minus, errors_plus, loss_mean, loss_minus, loss_plus))
    output = output.T


    # in case subsamping is needed for large files 
    subs_indices = np.random.choice(df.index, int(output.shape[0]/100*10), replace=False)
    subs_indices.sort()
    output = output[subs_indices]
   
    
    csv_file_name="MSPQN_bfgs.csv"
    pd.DataFrame(output).to_csv(csv_file_name, header  = ['epoch', 'g_evals', 'errors_mean', 'errors_minus', 'errors_plus', 'loss_mean', 'loss_minus', 'loss_plus'])   








