import sys
sys.setrecursionlimit(1000000)
import os
import logging
import argparse
import glob
import matplotlib.pyplot as plt
import numpy as np
import scipy.stats

# to ignore warnings ...
import warnings
warnings.filterwarnings('ignore')

logging.disable(logging.WARNING)
os.environ["TF_CPP_MIN_LOG_LEVEL"] = "3"

# Just disables the warning, doesn't enable AVX/FMA
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'

import time
import pandas as pd
import time


def replace(group):
    mean, std = group.mean(), group.std()
    outliers = (group - mean).abs() > 3*std
    group[outliers] = mean        # or "group[~outliers].mean()"
    return group


def mean_confidence_interval(data, confidence=0.95):
    a = np.array(data)
    n = len(a)

    m = data
    se = 1


    # m, se = np.mean(a, axis=0), scipy.stats.sem(a, axis=0)
    h = se * scipy.stats.t.ppf((1 + confidence) / 2., n-1)
    return m, m-h, m+h


if __name__ == '__main__':

    # file_names = glob.glob('MSPQN_1/Summary_seed_*_lrl_local_0.005_num_local_steps_10max_epochs1000_*ref_4*_num_subdomains_4_*.csv')   
    file_names = glob.glob('ASPQN_1/Summary_seed_125_opt_type_1_lr_global_1.0_lrl_local_1.0_num_local_steps_100max_epochs1000_num_points_x_25_num_points_y_50_num_ref_4_num_levels_1_num_subdomains_6_ada_net_False_T_4.0_history_3_use_adaptive_activation_True.csv')   


    # pd.set_option('display.max_columns', None)  # or 1000
    # pd.set_option('display.max_rows', None)  # or 1000
    # pd.set_option('display.max_colwidth', None)  # or 199

    for file_name in file_names:
        print("file_name  ", file_name)


    losses=None
    errors=None

    for file_name in file_names:

    	df = pd.read_csv (file_name)
    	df = df.sample(frac=0.1, replace=False, random_state=1).sort_index()
    	
    	csv_file_name = "aSPQN_bfgs_s6_l100.csv"
    	df.to_csv(csv_file_name, sep=',')




    # in case subsamping is needed for large files 
    # subs_indices = np.random.choice(df.index, int(output.shape[0]/100*10), replace=False)
    # subs_indices.sort()
    # output = output[subs_indices]
   
        # csv_file_name="aSPQN_bfgs_s2_l10.csv"
        # df.to_csv(csv_file_name, sep=',')










