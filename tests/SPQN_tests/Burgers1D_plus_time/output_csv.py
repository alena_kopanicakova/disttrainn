import pandas as pd
import time
import warnings
import scipy.stats
import numpy as np
import matplotlib.pyplot as plt
import glob
import argparse
import logging
import os
import sys
sys.setrecursionlimit(1000000)

# to ignore warnings ...
warnings.filterwarnings('ignore')

logging.disable(logging.WARNING)
os.environ["TF_CPP_MIN_LOG_LEVEL"] = "3"

# Just disables the warning, doesn't enable AVX/FMA
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'


def replace(group):
    mean, std = group.mean(), group.std()
    outliers = (group - mean).abs() > 3*std
    group[outliers] = mean        # or "group[~outliers].mean()"
    return group


def mean_confidence_interval(data, confidence=0.95):
    a = np.array(data)
    n = len(a)

    mean_ = np.mean(a, axis=0)
    std_ = np.std(a, axis=0)
    # h = se * scipy.stats.t.ppf((1 + confidence) / 2., n-1)

    # m = data
    # se = 1

    # m, se = np.mean(a, axis=0), scipy.stats.sem(a, axis=0)
    # h = se * scipy.stats.t.ppf((1 + confidence) / 2., n-1)
    return mean_, mean_-std_, mean_+std_


def ploting_for_sd_numsteps(i_locsteps, j_sd):
    filestring = 'ASPQN_1/Summary_seed_*_opt_type_1_lr_global_1.0_lrl_local_1.0_num_local_steps_' + \
        str(i_locsteps)+'max_epochs*_num_points_x_25_num_points_y_50_num_ref_4_num_levels_1_num_subdomains_' + \
        str(j_sd) + \
        '_ada_net_False_T_4.0_history_3_use_adaptive_activation_True.csv'
    print(filestring)

    file_names = glob.glob(filestring)

    for file_name in file_names:
        print("file_name  ", file_name)

    losses = None
    errors = None

    for file_name in file_names:

        df = pd.read_csv(file_name)

        loss = df['loss_train']
        error = df['L2_error_rel']
        g_evals = df['g_evals']
        indices = df['epoch']

        if (errors is None):
            errors = error
            losses = loss
        else:
            errors = np.vstack((errors, error))
            losses = np.vstack((losses, loss))

    errors_mean, errors_minus, errors_plus = mean_confidence_interval(
        errors)
    loss_mean, loss_minus, loss_plus = mean_confidence_interval(losses)

    indices = indices.to_numpy()
    indices = indices.reshape(errors_mean.shape)

    g_evals = g_evals.to_numpy()
    g_evals = g_evals.reshape(errors_mean.shape)

    output = np.vstack((indices, g_evals, errors_mean,  errors_minus,
                       errors_plus, loss_mean, loss_minus, loss_plus))
    output = output.T

    # in case subsamping is needed for large files
    # subs_indices = np.random.choice(df.index, int(
    #     output.shape[0]/100*10), replace=False)
    # subs_indices.sort()
    # output = output[subs_indices]
    fileout = 'ASPQN_bfgs_Klein_Gordon_subdomains_' + \
        str(j_sd)+'_num_local_steps_'+str(i_locsteps)
    csv_file_name = fileout+".csv"
    png_file_name = fileout+".png"

    pd.DataFrame(output).to_csv(csv_file_name, header=[
        'epoch', 'g_evals', 'errors_mean', 'errors_minus', 'errors_plus', 'loss_mean', 'loss_minus', 'loss_plus'], index=False)
    # pd.DataFrame(output).to_csv(csv_file_name, header  = ['g_evals','errors_mean', 'loss_mean'],index=False)

    plt.plot(g_evals, errors_mean, color='b')
    plt.fill_between(g_evals, errors_plus,
                     errors_minus, color='b', alpha=.1)
    plt.plot(g_evals, loss_mean, color='r')
    plt.fill_between(g_evals, loss_plus, loss_minus,
                     color='r', alpha=.1)
    plt.legend(['errors_mean', 'error conf', 'loss_mean',
               'loss conf'], loc='best', shadow=True)

    plt.ylabel('mean')
    plt.xlabel('g_evals')
    plt.yscale('log')
    # plt.xscale('log')
    plt.grid()
    plt.title(fileout)
    plt.ylim(5e-8, 5)
    plt.savefig(png_file_name)


if __name__ == '__main__':

    # file_names = glob.glob('MSPQN_1/Summary_seed_*_lrl_local_0.005_num_local_steps_10max_epochs1000_*ref_4*_num_subdomains_4_*.csv')
    num_local_steps = [10, 50, 100]
    num_subdomains = [2, 3, 6]
    for i_locsteps in num_local_steps:
        for j_sd in num_subdomains:
            plt.figure(j_sd*i_locsteps)
            ploting_for_sd_numsteps(i_locsteps=i_locsteps, j_sd=j_sd)

            


