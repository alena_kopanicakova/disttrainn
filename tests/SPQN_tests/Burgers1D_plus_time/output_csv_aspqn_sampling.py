import pandas as pd
import time
import warnings
import scipy.stats
import numpy as np
import matplotlib.pyplot as plt
import glob
import argparse
import logging
import os
import sys
sys.setrecursionlimit(1000000)

# to ignore warnings ...
warnings.filterwarnings('ignore')

logging.disable(logging.WARNING)
os.environ["TF_CPP_MIN_LOG_LEVEL"] = "3"

# Just disables the warning, doesn't enable AVX/FMA
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'


def replace(group):
    mean, std = group.mean(), group.std()
    outliers = (group - mean).abs() > 3*std
    group[outliers] = mean        # or "group[~outliers].mean()"
    return group


def mean_confidence_interval(data, confidence=0.95):
    a = np.array(data)
    n = len(a)

    mean_ = np.mean(a, axis=0)
    std_ = np.std(a, axis=0)
    return mean_, mean_-std_, mean_+std_


def ploting_for_sd_numsteps(i_locsteps, j_sd, k_ref):
    filestring = 'ASPQN_1/Sum_seed_*_opt_type_1_lr_global_1.0_lrl_local_1.0_num_local_steps_' + \
        str(i_locsteps)+'max_epochs*_num_points_x_25_num_points_y_50_num_ref_'+str(k_ref)+'_num_levels_1_width_20_hiddenlayers_coarse_6_num_subdomains_' + \
        str(j_sd) + \
        '_ada_net_False_T_*_history_3_use_adaptive_activation_True.csv'
    print(filestring)

    

    file_names = glob.glob(filestring)

    print("\n \n i_locsteps ", i_locsteps, "  j_sd  ", j_sd, "   k_ref   ", k_ref)

    for file_name in file_names:
        print("file_name  ", file_name)

    losses = None
    errors = None



    for file_name in file_names:

        df = pd.read_csv(file_name)

        loss = df['loss_train']
        error = df['L2_error_rel']
        g_evals = df['g_evals']
        epochs = df['epoch']

        if (errors is None):
            errors = error
            losses = loss
        else:
            errors = np.vstack((errors, error))
            losses = np.vstack((losses, loss))


    if(len(errors.shape)==1):
        errors_mean, errors_minus, errors_plus  =   errors, errors, errors
        loss_mean, loss_minus, loss_plus        =   losses, losses, losses

    else:
        errors_mean, errors_minus, errors_plus = mean_confidence_interval(
            errors)
        loss_mean, loss_minus, loss_plus = mean_confidence_interval(losses)


    epochs = epochs.to_numpy()
    epochs = epochs.reshape(errors_mean.shape)

    g_evals = g_evals.to_numpy()
    g_evals = g_evals.reshape(errors_mean.shape)

    output = np.vstack((epochs, g_evals, errors_mean,  errors_minus,
                       errors_plus, loss_mean, loss_minus, loss_plus))
    output = output.T

    
    memory_m = 3
    local_UC = 4*memory_m*i_locsteps
    UC  = 4*memory_m*epochs + 2*epochs + local_UC*epochs/j_sd



    output = np.vstack((epochs, g_evals, UC, errors_mean,  errors_minus,
                       errors_plus, loss_mean, loss_minus, loss_plus))
    output = output.T


    csv_file_name="ASPQN_locSteps"+str(i_locsteps)+"_sbds"+str(j_sd)+ "_ref"+str(k_ref)+".csv"


    print("csv_file_name: ", csv_file_name)


    # subsampling in case of too many outputs 
    num_subs_entries = 300
    if(output.shape[0] > num_subs_entries):
        subs_indices = np.random.choice(df.index, int(num_subs_entries), replace=False)
        subs_indices.sort()
        output = output[subs_indices]


    pd.DataFrame(output).to_csv(csv_file_name, header  = ['epoch',  'g_evals', 'UC', 'errors_mean', 'errors_minus', 'errors_plus', 'loss_mean', 'loss_minus', 'loss_plus'])   




if __name__ == '__main__':

    # file_names = glob.glob('MSPQN_1/Summary_seed_*_lrl_local_0.005_num_local_steps_10max_epochs1000_*ref_4*_num_subdomains_4_*.csv')
    num_local_steps = [10]
    num_subdomains = [8]
    num_ref = [1, 2, 3]
    for i_locsteps in num_local_steps:
        for j_sd in num_subdomains:
            for k_ref in num_ref:
                ploting_for_sd_numsteps(i_locsteps=i_locsteps, j_sd=j_sd, k_ref=k_ref)

            


