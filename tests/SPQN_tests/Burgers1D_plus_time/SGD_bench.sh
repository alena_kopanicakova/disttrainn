#!/bin/bash
mkdir SGD

for seed in 1 13 125 532 987;
do
	for num_points_x  in 25;
	do 	
		for num_points_y  in 50;
		do 			
			for num_ref in 1 4;
			do
				for ada_net in f;
				do				
					for max_epochs in 1000 10000 100000;
					do 
						for lr_global  in 1.0 0.5 1e-1 5e-2 1e-2 5e-3 1e-3 5e-4 1e-4;
						do 				
							for opt_type in 3;
							do 				
								for history in 1;
								do 		
									for hiddenlayers_coarse in 6;
									do 		
										for width in 20;
										do 												
											for T in 6;
											do 												
												for ada_fun in t;
												do 		
													for num_levels in 1;
													do 																																				
														sbatch --output=SGD/SGD${lr_global}_seed${seed}_opt_type${opt_type}_history${history}_num_points_x${num_points_x}_num_points_y${num_points_y}_num_ref${num_ref}_ada_net${ada_net}_max_epochs${max_epochs}_T${T}_ada_fun${ada_fun}_numlayers_${hiddenlayers_coarse}_width${width}_num_levels${num_levels}.out  --job-name=SGD submission_script_serial.job $seed $num_points_x $num_points_y $num_ref $ada_net $max_epochs $lr_global $opt_type $history $hiddenlayers_coarse $width $T $ada_fun $num_levels; 
													done
												done
											done
										done
									done
								done
							done
						done
					done
				done
			done
		done
	done
done