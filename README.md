[![License](https://img.shields.io/badge/License-BSD%203--Clause-blue.svg)](https://opensource.org/licenses/BSD-3-Clause)


# Additive and multiplicative QN preconditioners for training Physics-informed neural networks (PINNs) and Operator learning approaches (DeepONets)
This repository contains the code used to generate results in the paper: <br> 
**Enhancing training of physics-informed neural networks using domain-decomposition based preconditioning strategies by Kopanicakova, Kothari, Karniadakis, Krause.**<br> 

If you use the developed code/its components for your research, please use the following bibtex entries (or equivalent) to cite us
```bibtex
@article{kkkk_24,
title = {Enhancing Training of Physics-Informed Neural Networks Using Domain Decomposition–Based Preconditioning Strategies},
author = {Alena Kopani{\v{c}}{\'a}kov{\'a} and Hardik Kothari and George Em Karniadakis and Rolf Krause},
journal = {SIAM Journal on Scientific Computing},
volume = {0},
number = {0},
pages = {S46-S67},
year = {0},
doi = {10.1137/23M1583375},
URL = {https://epubs.siam.org/doi/abs/10.1137/23M1583375},
note = {arXiv preprint arXiv:2306.17648},
}
```

```bibtex
@misc{distrainngit,
	author = {Alena Kopani{\v c}{\'a}kov{\'a} and Youngkyu Lee and Hardik Kothari},
	title = {{DistTraiNN: Model parallel framework for distributed training of scientific machine-learning applications. {G}it repository}},
	url = {https://bitbucket.org/alena_kopanicakova/disttrainn},
	howpublished = {https://bitbucket.org/alena_kopanicakova/disttrainn},
	year = {2023}
}
```


## Depedencies:
- Pytorch
- Pandas


## Examples
Scripts used to obtain numerical results reported in the paper can be found in folder **tests/SPQN_tests**

### Code execution:
1. 	mkdir DD_train; cd DD_train
2. 	git clone git@bitbucket.org:alena_kopanicakova/disttrainn.git
3. 	cd disttrainn
4. 	export path, e.g., <br />
	export PYTHONPATH="${PYTHONPATH}:/Users/alenakopanicakova/code/DD_PINN/disttrainn"
5. run experiment, e.g., <br />
	cd tests/SPQN_tests/AllenCahn <br />
	python3 test_SPQN_multiplicative.py --num_points_x 25 --num_points_y 25 --num_ref 2 --num_subdomains 6 --lr_global 1.0 



<!-- #### Parallel execution:
-  -->

## Dislaimer
This code was developed for research purposes only. The authors make no warranties, express or implied, regarding its suitability for any particular purpose or its performance.

## License
The software is realized with NO WARRANTY and it is licenzed under [BSD 3-Clause license](https://opensource.org/licenses/BSD-3-Clause)

# Copyright
Copyright (c) 2023 Institute of Computational Science - USI Università della Svizzera Italiana

## Contact
Alena Kopaničáková (<alena_kopanicakova@brown.edu>, <alena.kopanicakova@usi.ch>)

## Contributors: 
* Alena Kopanicakova (Brown, Providence and USI, Lugano)
* Youngkyu Lee (Brown, Providence)
* Hardik Kothari (USI, Lugano)